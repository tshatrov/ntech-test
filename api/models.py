from django.db import models
from django.db.models import F

# Create your models here.

class Person(models.Model):
    name = models.CharField(max_length=100)
    x = models.IntegerField()
    y = models.IntegerField()

    def __str__(self):
        return '{} ({},{})'.format(self.name, self.x, self.y)

    def to_json(self):
        return {
            'name': self.name,
            'x': self.x,
            'y': self.y,
        }

    @classmethod
    def get_nearest(cls, x, y, n):
        return cls.objects.annotate(
            dist=(F('x') - x) * (F('x') - x) + (F('y') - y) * (F('y') - y)
        ).order_by('dist')[:n]
