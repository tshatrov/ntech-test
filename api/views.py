from django.shortcuts import render
from django.views import View
from django import forms
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from .models import Person


class PersonForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = ['name', 'x', 'y']


class PersonView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(PersonView, self).dispatch(*args, **kwargs)

    def post(self, request):
        form = PersonForm(request.POST)
        if form.is_valid():
            new_person = form.save()
            return JsonResponse({"success": True, "person": new_person.to_json()})
        else:
            return JsonResponse({"success": False, "errors": dict(form.errors.items())}, status=400)


class NearestForm(forms.Form):
    x = forms.IntegerField(label="X")
    y = forms.IntegerField(label="Y")
    n = forms.IntegerField(min_value=1, max_value=100)


class NearestView(View):
    def get(self, request):
        f = NearestForm(request.GET)
        if f.is_valid():
            x = f.cleaned_data['x']
            y = f.cleaned_data['y']
            n = f.cleaned_data['n']
            return JsonResponse(
                {"success": True, "neighbors": [p.to_json() for p in Person.get_nearest(x, y, n)]},
                safe=False
            )
        else:
            return JsonResponse({"success": False, "errors": dict(f.errors.items())}, status=400)
